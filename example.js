const RFID = require('./');

// check if pcscd is available or not
if(!RFID){
	console.error('pcscd service is not running!', RFID);
	return;
}

(async () => {
	await RFID.setTimeout(0xff);

	RFID.on('reader', received => {
		console.log('reader', received);
	});

	RFID.on('card-status', received => {
		console.log('card status', received);
	});

	RFID.on('status', received => {
		console.log('reader status', received);
	});

	RFID.on('error', received => {
		console.log('error', received);
	});

	RFID.on('data', received => {
		console.log('data', received);
	});

	RFID.on('blocks', received => {
		console.log('blocks', received);
	});

	RFID.on('removed', received => {
		console.log('card removed', received);
	});

	RFID.on('uid', received => {
		console.log('card UID', received);
	});

	RFID.on('end', received => {
		console.log('Reader Ended', received);
	});

	RFID.on('ready', async protocol => {
		console.log('card ready', protocol);
		const obj = {
			id: '12345678',
			time: Date.now(),
		};

		const input = '001415';
		const block = 4;

		/** * /
		const write = await RFID.writeBlocks(input, block);

		console.log('--------- Data Written ---------');
		console.log('Input :', input);
		console.log('Write :', write);
		/** */

		const read = [
			await RFID.readBlocks(block),
			await RFID.readBlocksString(block),
			await RFID.readBlocksJSON(block),
		];

		const status = await RFID.status(block);

		console.log('--------- Data Read ---------');
		console.log('RAW', read[0]);
		console.log('STRING', read[1]);
		console.log('JSON', read[2]);
		console.log('--------- ---- ---- ---------');
	});
})();
