# pcsclite Helper

this package is only a wrapper for [pcsclite](https://www.npmjs.com/package/pcsclite). Only tested on Ubuntu/Debian using ACR122U and MIFARE 1K card.

## Prerequisites
You must install these packages first
```shell
sudo apt install libpcsclite1 libpcsclite-dev pcscd libacsccid1
```
__NOTE:__ libacsccid1 is required for ACR122U reader

these kernel modules must be removed in order to make pcsc reader detected:
```shell
# remove kernel modules temporarily
sudo rmmod pn533_usb pn533 nfc

# block kernel modules permanently
echo "install nfc /bin/false" | sudo tee -a /etc/modprobe.d/blacklist.conf
echo "install pn533 /bin/false" | sudo tee -a /etc/modprobe.d/blacklist.conf
echo "install pn533_usb /bin/false" | sudo tee -a /etc/modprobe.d/blacklist.conf
```

You can execute `enablepcsc.sh`, included in this repository, to simplify the steps above.

## Installation
```shell
npm install @iqrok/pcsclite.helper
```

## Example
```javascript
const RFID = require('@iqrok/pcsclite.helper');

// check if pcscd is available or not
if(!RFID){
	console.error('pcscd service is not running!', RFID);
	return;
}

(async () => {
	await RFID.setTimeout(0xff);

	RFID.on('reader', received => {
		console.log('reader', received);
	});

	RFID.on('status', received => {
		console.log('status', received);
	});

	RFID.on('error', received => {
		console.log('error', received);
	});

	RFID.on('data', received => {
		console.log('data', received);
	});

	RFID.on('blocks', received => {
		console.log('blocks', received);
	});

	RFID.on('removed', received => {
		console.log('card removed', received);
	});

	RFID.on('uid', received => {
		console.log('card uid', received);
	});

	RFID.on('ready', async protocol => {
		const obj = {
			id: '12345678',
			time: Date.now(),
		};

		const input = JSON.stringify(obj);
		const block = 4;

		const write = await RFID.writeBlocks(input, block);

		const read = [
			await RFID.readBlocks(block),
			await RFID.readBlocksString(block),
			await RFID.readBlocksJSON(block),
		];

		const status = await RFID.status(block);

		console.log('--------- Data Written ---------');
		console.log('Input :', input);
		console.log('Write :', write);
		console.log('--------- Data Read ---------');
		console.log('RAW', read[0]);
		console.log('STRING', read[1]);
		console.log('JSON', read[2]);
		console.log('--------- ---- ---- ---------');
	});
})();
```

## Events
|Event Name|Triggered on|
|---|---|
|reader|reader status is changed|
|status|reader connection status is changed|
|card-status|request RFID card status|
|error|there's error in reader or card action|
|data|incoming data from RFID card|
|blocks|incoming data after reading multiple blocks|
|ready|RFID card is connected to reader|
|removed|RFID card is removed from reader|
|uid|RFID card uid|
