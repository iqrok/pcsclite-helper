/**
 * @typedef {Object} Response
 * @property {boolean} status - action status
 * @property {*} data - action data
 */

/**
 * @typedef {Object} CardStatusCode
 * @property {number} code - code number
 * @property {string} message - text from code number
 */

/**
 * @typedef {Object} CardStatus
 * @property {boolean} status - action status
 * @property {Object} data - action data
 * @property {byte[]} data.raw - raw data
 * @property {CardStatusCode} data.error - error status
 * @property {number} data.field - external RF field
 * @property {number} data.numOfTargets - number if targets, default = 1
 * @property {number} data.logicalNumber - logical number
 * @property {CardStatusCode} data.RxBr - Rx Bitrates
 * @property {CardStatusCode} data.TxBr - Tx Bitrates
 * @property {CardStatusCode} data.type - modulo type
 */

'use strict'

const eventEmitter = require('events');
let pcsc;

// if pcscd service is unavailable, then stop immediately
try{
	pcsc = (require('pcsclite'))();
} catch (error) {
	module.exports = null;
	return;
}

/***** Constant values retrieved from datasheet *****/
const _CONSTANTS = {
	HEADERS: {
		read: [0xFF, 0xB0, 0x00],
		write: [0xFF, 0xD6, 0x00],
	},
	getData: [0xFF, 0xCA, 0x00, 0x00, 0x00],
	status: [0xFF, 0x00, 0x00, 0x00, 0x02, 0xD4, 0x04],
	timeout: [0xFF, 0x00, 0x41, 0x01, 0x00],
	loadKey: [0xFF, 0x82, 0x00, 0x00, 0x06, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0],
	authenticate: [0xFF, 0x86, 0x00, 0x00, 0x05, 0x01, 0x00, 16, 0x60, 0x01],
};

const _ERRORS = {
	0x00: 'No Error',
	0x01: 'Time Out, the target has not answered',
	0x02: 'A CRC error has been detected by the contactless UART',
	0x03: 'A Parity error has been detected by the contactless UART',
	0x04: 'an erroneous Bit Count has been detected',
	0x05: 'Framing error during MIFARE operation',
	0x06: 'An abnormal bit-collision has been detected during bit wise anti-collision at 106 Kbps',
	0x07: 'Communication buffer size insufficient',
	0x08: 'RF Buffer overflow has been detected by the contactless UART',
	0x10: 'Invalid parameter (range, format, ...)',
	0x12: 'DEP Protocol: The chip configured in target mode does not support the command received',
	0x13: 'DEP Protocol / MIFARE / ISO/IEC 14443-4: The data format does not match to the specification',
	0x14: 'MIFARE: Authentication error',
	0x23: 'ISO/IEC 14443-3: UID Check byte is wrong',
	0x25: 'DEP Protocol: Invalid device state, the system is in a state which does not allow the operation',
	0x26: 'Operation not allowed in this configuration (host controller interface)',
	0x27: 'This command is not acceptable due to the current context of the chip',
	0x29: 'The chip configured as target has been released by its initiator',
	0x2A: 'the ID of the card does not match',
	0x2B: 'the card previously activated has disappeared',
	0x2C: 'Mismatch between the NFCID3 initiator and the NFCID3 target',
	0x2D: 'An over-current event has been detected',
	0x2E: 'NAD missing in DEP frame',
};

const _BITRATES = {
	0x00: '106 Kbps',
	0x01: '212 Kbps',
	0x02: '424 Kbps',
};

const _TYPES = {
	0x00: 'ISO 14443 or MIFARE',
	0x10: 'FeliCa',
	0x01: 'Active Mode',
	0x02: 'Inoovision Jewel Tag',
};

const _STANDARDS = {
	0x00: {name: 'UNKNOWN', type: null},
	0x01: {name: 'ISO 14443A, level 1', type: 'A'},
	0x02: {name: 'ISO 14443A, level 2', type: 'A'},
	0x03: {name: 'ISO 14443A, level 3/4', type: 'A'},
	0x05: {name: 'ISO 14443B, level 1', type: 'B'},
	0x06: {name: 'ISO 14443B, level 2', type: 'B'},
	0x07: {name: 'ISO 14443B, level 3/4', type: 'B'},
	0x09: {name: 'ICODE1', type: null},
	0x0b: {name: 'ISO 15693', type: null},
};

const _CARDS = {
	0x0001: 'NXP Mifare Standard 1k',
	0x0002: 'NXP Mifare Standard 4k',
	0x0003: 'NXP Mifare UltraLight',
	0x0006: 'ST MicroElectronics SR176',
	0x0007: 'ST MicroElectronics SRI4K, SRIX4K, SRIX512, SRI512, SRT512 ≥ 1.55',
	0x000A: 'Atmel AT88SC0808CRF',
	0x000B: 'Atmel AT88SC1616CRF',
	0x000C: 'Atmel AT88SC3216CRF',
	0x000D: 'Atmel AT88SC6416CRF',
	0x0012: 'Texas Intruments TAG IT',
	0x0013: 'ST MicroElectronics LRI512',
	0x0014: 'NXP ICODE SLI',
	0x0016: 'NXP ICODE1',
	0x0021: 'ST MicroElectronics LRI64',
	0x0024: 'ST MicroElectronics LR12',
	0x0025: 'ST MicroElectronics LRI128',
	0x0026: 'NXP Mifare Mini',
	0x002F: 'Innovision Jewel',
	0x0030: 'Innovision Topaz (NFC Forum type 1 tag)',
	0x0034: 'Atmel AT88RF04C',
	0x0035: 'NXP ICODE SL2',
	0x003A: 'NXP Mifare UltraLight C',
	0xFFA0: 'Generic/unknown 14443-A card',
	0xFFA1: 'Kovio RF barcode ≥ 1.63',
	0xFFB0: 'Generic/unknown 14443-B card',
	0xFFB1: 'ASK CTS 256B',
	0xFFB2: 'ASK CTS 512B',
	0xFFB3: 'Pre-standard ST MicroElectronics SRI 4K < 1.55',
	0xFFB4: 'Pre-standard ST MicroElectronics SRI X512 < 1.55',
	0xFFB5: 'Pre-standard ST MicroElectronics SRI 512 < 1.55',
	0xFFB6: 'Pre-standard ST MicroElectronics SRT 512 < 1.55',
	0xFFB7: 'Inside Contactless PICOTAG/PICOPASS',
	0xFFB8: 'Generic Atmel AT88SC / AT88RF card',
	0xFFC0: 'Calypso card using the Innovatron protocol',
	0xFFD0: 'Generic ISO 15693 from unknown manufacturer',
	0xFFD1: 'Generic ISO 15693 from EMMarin (or Legic)',
	0xFFD2: 'Generic ISO 15693 from ST MicroElectronics, block number on 8 bits',
	0xFFD3: 'Generic ISO 15693 from ST MicroElectronics, block number on 16 bits',
	0xFFFF: 'Virtual card (test only)',
};

/**
 *	delay in millisecons. Must use await
 *	@private
 *	@param {number} ms - milliseconds to wait
 * */
function sleep(ms){
	return new Promise(resolve => setTimeout(resolve, ms))
};

/**
 *	Parse action status according to datasheet
 *	@private
 *	@param {number} status - status code to be parsed
 *	@returns {boolean}
 * */
function formatStatus(status){
	status = status.readUInt16BE();

	if(status === 0x9000){
		return true;
	}

	if(status === 0x6300){
		return false;
	}

	return null;
}

/**
 *	Parse received bytes
 *	@private
 *	@param {byte[]} received - array of bytes to be parsed
 *	@returns {Response} parsed data
 * */
function formatReceived(received){
	const length = received.length;
	const data = received.slice(0, length-2);
	const status = formatStatus(received.slice(length-2, length));

	return {status, data};
}

class __pcsc_helper extends eventEmitter{
	constructor(){
		super();
		this.init();
	}

	/**
	 *	init pcsc instance and register all events
	 *	@private
	 * */
	init(){
		const self = this;

		self._reader = undefined;
		self._protocol = undefined;
		self._card = {};
		self.isConnected = false;

		self._expectedLength = 128; // default max length
		self._bytePerBlock = 0x10; // block length is 16-bytes
		self._UIDLength = 16; // expecred UID Length

		pcsc.on('reader', function(reader) {
			self._reader = reader;

			reader.on('error', function(err) {
				self._emit('error', err);
			});

			self.isConnected = true;
			self._emit('status', {
					name: reader.name,
					status: self.isConnected,
				});

			reader.on('status', function(status) {
				self._card = {
					standard: _STANDARDS[status.atr[12]],
					name: _CARDS[(status.atr[13] << 8) | status.atr[14]],
				};

				self._emit('reader', {
						...status,
						...self._card,
					});

				const changes = this.state ^ status.state;

				if (changes) {
					if ((changes & this.SCARD_STATE_EMPTY) && (status.state & this.SCARD_STATE_EMPTY)) {
						reader.disconnect(reader.SCARD_LEAVE_CARD, function(err) {
							if (err) {
								self._emit('error', err);
							} else {
								self._emit('removed');
								self._protocol = undefined;
								self._card = {};
							}
						});
					} else if ((changes & this.SCARD_STATE_PRESENT) && (status.state & this.SCARD_STATE_PRESENT)) {
						reader.connect({ share_mode : this.SCARD_SHARE_SHARED }, function(err, protocol) {
							console.log('uid');
							if (err) {
								self._emit('error', err);
							} else {
								self._protocol = protocol;
								self._emit('ready', protocol);
								self.readUID();
							}
						});
					}
				}
			});

			reader.on('end', function() {
				self.isConnected = false;
				self._emit('status', {
						name: reader.name,
						status: self.isConnected,
					});
				self._emit('end', `Reader ${this.name} removed`);
			});
		});

		pcsc.on('error', function(err) {
			self._emit('error', err);
		});
	};

	/**
	 *	emit data if only there is at least 1 listener
	 *	@private
	 *	@param {string} eventName - event name to be emitted
	 *	@param {*} value - event data, any datatype can be emitted
	 * */
	_emit(eventName, value){
		const self = this;

		if(self.listenerCount(eventName) > 0){
			self.emit(eventName, value);
		}
	};

	/**
	 *	pad buffer with 0x00 if length < 16
	 *	@private
	 *	@param {byte[]} _buffer - buffer to be padded
	 *	@returns {bytes[]} padded buffer
	 * */
	_padBuffer(_buffer){
		const _modulo = _buffer.length % 0x10;

		if(_modulo === 0){
			return _buffer;
		}

		const padLength = 0x10 - _modulo;
		const _padder = [];
		for(let counter = 0; counter < padLength; counter++){
			_padder.push(0x00);
		}

		return Buffer.from([..._buffer, ..._padder]);
	}

	/**
	 *	write into a block
	 *	@private
	 *	@param {string|byte[]} data - written data
	 *	@param {number} [_startingBlock=0x04] - first block where data will be written
	 *	@returns {Response} write status and written buffer
	 * */
	_write(data, _startingBlock = 0x04){
		const self = this;

		if(!self._reader || !self._protocol){
			return -1;
		}

		data = self._padBuffer(Buffer.from(data));
		const length = data.length;
		const expectedLength = length + 2;

		return new Promise(resolve => {
			const _buffer = Buffer.from([ ..._CONSTANTS.HEADERS.write, _startingBlock, length, ...data]);

			self._reader.transmit(_buffer, 2, self._protocol, function(err, data) {
					if (err) {
						self._emit('error', err);
						resolve([-1, err]);
					} else {
						const value = {action: 'write', ...formatReceived(data), _buffer};
						self._emit('data', value);
						resolve(value);
					}
				});
		});
	};

	/**
	 *	read card UID
	 *	@returns {Response} card UID buffer
	 * */
	readUID(){
		const self = this;

		if(!self._reader || !self._protocol){
			return -1;
		}

		return new Promise(resolve => {
			const expectedLength = self._UIDLength;
			const _buffer = Buffer.from(_CONSTANTS.getData);

			self._reader.transmit(_buffer, expectedLength, self._protocol, function(error, data) {
					if (error) {
						self._emit('error', error);
						resolve({ status: false, error });
						return;
					}

					self._emit('uid', data);
					resolve({ status: true, data });
				});
		});
	};

	/**
	 *	set buffer length for card UID
	 *	@params {number} bytes - expected buffer length in bytes
	 * */
	setUIDLength(bytes = 16){
		const self = this;
		self._UIDLength = bytes;
	};

	/**
	 *	read a block
	 *	@private
	 *	@param {number} length - number of byte to read
	 *	@param {number} [_startingBlock=0x04] - first block where data will be read
	 *	@returns {Response} read status and buffer
	 * */
	_read(length = 16, _startingBlock = 0x04){
		const self = this;

		if(!self._reader || !self._protocol){
			return -1;
		}

		const expectedLength = length + 2;
		return new Promise(resolve => {
			const _buffer = Buffer.from([ ..._CONSTANTS.HEADERS.read, _startingBlock, length]);

			self._reader.transmit(_buffer, expectedLength, self._protocol, function(err, data) {
					if (err) {
						self._emit('error', err);
						resolve([-1, err]);
					} else {
						const value = {action: 'read', ...formatReceived(data), _buffer};
						self._emit('data', value);
						resolve(value);
					}
				});
		});
	};

	/**
	 *	write into multiple blocks
	 *	@param {string|byte[]} data - written data
	 *	@param {number} [_startingBlock=0x04] - first block where data will be written
	 *	@param {number} [_startingBlock=0x04] - first block where data will be written
	 *	@returns {Response} write status and written buffer
	 * */
	async writeBlocks(data = '', _startingBlock = 4){
		const self = this;

		const _buffer = Buffer.from(data);
		const blockIndex = {
				current: _startingBlock,
				max: _startingBlock + self.getBlockSize(_buffer),
			};

		const response = {
			status: true,
			data: [],
		};

		for(let index = 0; blockIndex.current < blockIndex.max; blockIndex.current++){
			// authenticate on new sector
			if(blockIndex.current % 4 === 0){
				await self._authenticate(blockIndex.current);
			}

			// avoid overwriting trailing block
			if(blockIndex.current % 4 === 3){
				++blockIndex.max;
				continue;
			}

			const _block = _buffer.slice(index, index + self._bytePerBlock);
			const write = await self._write(_block, blockIndex.current);

			response.status = response.status && write.status;
			response.data.push(write._buffer);

			index += self._bytePerBlock;
		}

		return response;
	};

	/**
	 *	read multiple blocks
	 *	@param {number} [_startingBlock=0x04] - first block where data will be written
	 *	@param {number} [length=0] - number of block to read
	 *	@param {boolean} [_emitEvent=true] - emit event when action is finished
	 *	@returns {Response} read status and data
	 * */
	async readBlocks(_startingBlock = 4, length = 0, _emitEvent = true){
		const self = this;

		if(length <= 0){
			length = 4;
		}

		const blockIndex = {
				current: _startingBlock,
				max: _startingBlock + length,
			};

		const response = {
			status: true,
			data: [],
		};

		const raw = [];
		for(; blockIndex.current < blockIndex.max; blockIndex.current++){
			// authenticate on new sector
			if(blockIndex.current % 4 === 0){
				await self._authenticate(blockIndex.current);
			}

			// skip trailing block
			if(blockIndex.current % 4 === 3){
				continue;
			}

			const read = await self._read(self._bytePerBlock, blockIndex.current);
			response.status = response.status && read.status;

			try{
				if(!read.status){
					throw read;
				}

				raw.push(read.data);
			} catch (error) {
				if(self.debug){
					console.error('Read Blocks Error', error);
				}

				await self.status(blockIndex.current);

				break;
			}
		}

		response.data = Buffer.concat(raw);

		if(_emitEvent){
			self._emit('blocks', response);
			self._emit('data', {action: 'readBlocks', ...response});
		}

		return response;
	};

	/**
	 *	read multiple blocks and convert it into string
	 *	@param {number} [_startingBlock=0x04] - first block where data will be written
	 *	@param {number} [length=0] - number of block to read
	 *	@param {boolean} [_emitEvent=true] - emit event when action is finished
	 *	@returns {Response} read status and data
	 * */
	async readBlocksString(_startingBlock = 4, length = 0, _emitEvent = true){
		const self = this;

		const response = await self.readBlocks(_startingBlock, length, false);

		try{
			response.data = response.data
				.toString('utf8')
				.replace(/[^\x01-\x7F]/g, ''); // remove non-ascii character
		} catch (error) {
			if(self.debug){
				console.error('Failed at converting buffer to string', error);
			}
		}

		if(_emitEvent){
			self._emit('blocks', response);
			self._emit('data', {action: 'readBlocks', ...response});
		}

		return response;
	};

	/**
	 *	read multiple blocks and try to parse the string into object, if failed then return the string
	 *	@param {number} [_startingBlock=0x04] - first block where data will be written
	 *	@param {number} [length=0] - number of block to read
	 *	@param {boolean} [_emitEvent=true] - emit event when action is finished
	 *	@returns {Response} read status and data
	 * */
	async readBlocksJSON(_startingBlock = 4, length = 0, _emitEvent = true){
		const self = this;

		const response = await self.readBlocksString(_startingBlock, length, false);

		try{
			response.data = JSON.parse(response.data);
		} catch (error) {
			if(self.debug){
				console.error('Failed at parsing JSON', error);
			}
		}

		if(_emitEvent){
			self._emit('blocks', response);
			self._emit('data', {action: 'readBlocks', ...response});
		}

		return response;
	};

	/**
	 *	authenticate sector
	 *	@privaete
	 *	@param {number} [blockNumber=0x04] - block sector to be authenticated
	 *	@param {number} [location=0x00] - key location
	 *	@returns {Response} authenticate status and buffer
	 * */
	async _authenticate(blockNumber = 4, location = 0){
		const self = this;

		if(!self._reader || !self._protocol){
			return -1;
		}

		const payload = _CONSTANTS.authenticate;

		// set block number auth
		payload[7] = blockNumber;

		// set key type
		payload[8] = self._card.standard.type === 'B' ? 0x61 : 0x60;

		// set key location
		payload[9] = location;

		return new Promise(resolve => {
			self._reader.transmit(Buffer.from(payload), self._expectedLength, self._protocol, function(err, data) {
					if (err) {
						self._emit('error', err);
						resolve([-1, err]);
					} else {
						const value = {action: 'auth', ...formatReceived(data)};
						self._emit('data', value);

						resolve(value);
					}
				});
		});
	};

	/**
	 *	set timeout response from card
	 *	@param {number} [val=0xff] - timeout value according to datasheet
	 *	@returns {Response} action status and buffer
	 * */
	setTimeout(val = 0xff){
		const self = this;

		if(!self._reader || !self._protocol){
			return -1;
		}

		const payload = _CONSTANTS.timeout;

		// set timeout value
		payload[3] = val;

		return new Promise(resolve => {
			self._reader.transmit(Buffer.from(payload), self._expectedLength, self._protocol, function(err, data) {
					if (err) {
						self._emit('error', err);
						resolve([-1, err]);
					} else {
						const value = {action: 'setTimeout', ...formatReceived(data)};
						self._emit('data', value);

						resolve(value);
					}
				});
		});
	};

	/**
	 *	get last card status
	 *	@returns {CardStatus} status
	 * */
	status(){
		const self = this;

		if(!self._reader || !self._protocol){
			return -1;
		}

		return new Promise(resolve => {
			self._reader.transmit(Buffer.from(_CONSTANTS.status), self._expectedLength, self._protocol, function(err, data) {
					if (err) {
						self._emit('error', err);
						resolve([-1, err]);
					} else {
						const formatted = {
							status: data[2] === 0 ? true : false,
							data: {
								raw: data,

								error : {
									code: data[2],
									message: _ERRORS[data[2]],
								},

								field: data[3],
								numOfTargets: data[4],
								logicalNumber: data[5],
								RxBr: {
									code: data[6],
									message: _BITRATES[data[6]],
								},
								TxBr: {
									code: data[7],
									message: _BITRATES[data[7]],
								},
								type: {
									code: data[7],
									message: _TYPES[data[8]],
								},
							}
						}

						const value = {action: 'status', ...formatted};
						self._emit('card-status', formatted);
						self._emit('data', value);

						resolve(value);
					}
				});
		});
	};

	/**
	 *	get block size, which is multiplication from 16, from input
	 *	@param {byte[]|string|number} _bytes - input to be converted to block length
	 *	@returns {number} block length
	 * */
	getBlockSize(_bytes = 0){
		const self = this;
		const length = _bytes.length || _bytes;
		return Math.ceil(length / self._bytePerBlock);
	};

	/**
	 *	set debug status
	 *	@param {boolean|number} status - debug status value
	 *	@returns {boolean}
	 * */
	setDebug(status){
		const self = this;

		if(status === undefined){
			return false;
		}

		self.debug = +status;

		return true;
	};
};

module.exports = new __pcsc_helper();
