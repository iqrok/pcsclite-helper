#!/bin/sh

if [ $(id -u) -ne "0" ]; then
	echo "Please run as root"
	exit 1
fi

echo "blacklist pn533_usb" | tee -a /etc/modprobe.d/blacklist.conf
echo "blacklist pn533" | tee -a /etc/modprobe.d/blacklist.conf
echo "blacklist nfc" | tee -a /etc/modprobe.d/blacklist.conf
echo "install nfc /bin/false" | tee -a /etc/modprobe.d/blacklist.conf
echo "install pn533 /bin/false" | tee -a /etc/modprobe.d/blacklist.conf
echo "install pn533_usb /bin/false" | tee -a /etc/modprobe.d/blacklist.conf

if [ ! -z $(lsmod | grep -w pn533_usb) ]; then
	rmmod pn533_usb
fi

if [ ! -z $(lsmod | grep -w pn533) ]; then
	rmmod pn533
fi

if [ ! -z $(lsmod | grep -w nfc) ]; then
	rmmod nfc
fi

apt install libpcsclite1 libpcsclite-dev libacsccid1 pcscd

sleep 1

sed -i -r "s#(ExecStart=/usr/sbin/pcscd)(.*?)#\1 --foreground --debug --apdu --auto-exit#g" /lib/systemd/system/pcscd.service
systemctl daemon-reload
systemctl stop pcscd.socket && systemctl stop pcscd.service
systemctl start pcscd.socket && systemctl start pcscd.service
